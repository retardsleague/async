package hello;


import hello.domain.Customer;
import hello.domain.Post;
import hello.domain.User;

import java.util.ArrayList;
import java.util.List;

public class Response {
    
    private List<User> users = new ArrayList<>();
    private List<Post> posts = new ArrayList<>();
    private List<Customer> customers = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
