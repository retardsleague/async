package hello;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import hello.domain.Customer;
import hello.domain.Greeting;
import hello.domain.Post;
import hello.domain.User;
import hello.repoistory.CustomerRepositoryDelayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);

    private static final String template = "Hi %s!";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private UserLookupService userLookupService;

    @Autowired
    private PostLookupService postLookupService;

    @Autowired
    private CustomerRepositoryDelayer customerRepository;

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "there") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @RequestMapping("/async-test")
    public CompletableFuture<Response> run() throws Exception {
        long start = System.currentTimeMillis();
        Response response = new Response();

        CompletableFuture<List<User>> users = userLookup();
        CompletableFuture stage1 = users
                .thenAccept(response::setUsers)
                .thenRun(() -> logger.info("Users data is ready."));

        CompletableFuture<List<Post>> posts = users.thenApply(l -> postLookup(
                l.stream().map(User::getId).collect(Collectors.toList())).join());
        CompletableFuture stage2 = posts
                .thenAccept(response::setPosts)
                .thenRun(() -> logger.info("Posts data is ready."));

        
        CompletableFuture<List<Customer>> customers = customerLookup();
        CompletableFuture stage3 = customers
                .thenAccept(response::setCustomers)
                .thenRun(() -> logger.info("Customers data is ready."));

        //TODO remove stage1 ?
        CompletableFuture<Response> respFuture =
                CompletableFuture.allOf(stage1, stage2, stage3)
                        .thenRun(() -> logger.info("Response is ready."))
                        .thenApply((v) -> response);
        
        logger.info("Elapsed time: " + (System.currentTimeMillis() - start));
        return respFuture;
    }


    private CompletableFuture<List<Post>> postLookup(List<Long> userIds) {

        List<CompletableFuture<Post>> futuresList = userIds.stream()
                .map(postLookupService::findPost)
                .collect(Collectors.toList());

        CompletableFuture<Void> allFuturesResult = CompletableFuture.allOf(
                futuresList.toArray(new CompletableFuture[futuresList.size()]));

        return allFuturesResult.thenApply(
                v -> futuresList.stream ()
                        .map(CompletableFuture::join)
                        .collect(Collectors.<Post>toList()));
    }

    private CompletableFuture<List<User>> userLookup() throws InterruptedException {

        List<CompletableFuture<User>> futuresList = new ArrayList<>();
        futuresList.add(userLookupService.findUser(1L));
        futuresList.add(userLookupService.findUser(2L));
        futuresList.add(userLookupService.findUser(3L));

        CompletableFuture<Void> allFuturesResult = CompletableFuture.allOf(
                futuresList.toArray(new CompletableFuture[futuresList.size()]));

        return allFuturesResult.thenApply(
                v -> futuresList.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.<User>toList()));
    }
    
    private CompletableFuture<List<Customer>> customerLookup() throws InterruptedException {

        return customerRepository.findByLastName("Bauer");
    }
    
}