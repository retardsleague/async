package hello.repoistory;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import hello.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    
    //@Async
    //Annotation moved to CustomerRepositoryDelayerImpl to add delay
    CompletableFuture<List<Customer>> findByLastName(String lastName);
    
}