package hello.repoistory;


import hello.domain.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Repository
public class CustomerRepositoryDelayerImpl implements CustomerRepositoryDelayer {

    private static final Logger logger = LoggerFactory.getLogger(CustomerRepositoryDelayerImpl.class);
    
    @Autowired
    private CustomerRepository customerRepository;

    @Async
    @Override
    public CompletableFuture<List<Customer>> findByLastName(String lastName) throws InterruptedException {
        logger.info("Strarted Looking up " + lastName);
        CompletableFuture<List<Customer>> customers = customerRepository.findByLastName(lastName);
        
        Thread.sleep(2000L);
        logger.info("Finished Looking up " + lastName);
        return customers;
    }
    
}
