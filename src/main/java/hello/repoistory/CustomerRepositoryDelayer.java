package hello.repoistory;

import hello.domain.Customer;

import java.util.List;
import java.util.concurrent.CompletableFuture;


public interface CustomerRepositoryDelayer {

    CompletableFuture<List<Customer>> findByLastName(String lastName) throws InterruptedException;
    
}