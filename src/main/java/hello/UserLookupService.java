package hello;

import hello.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class UserLookupService {

    private static final Logger logger = LoggerFactory.getLogger(UserLookupService.class);

    @Autowired
    private RestTemplate restTemplate;


    @Async
    public CompletableFuture<User> findUser(Long userId) {
        logger.info("Strarted Looking up userId " + userId);
        String url = String.format("https://jsonplaceholder.typicode.com/users/%s", userId);
        User results = restTemplate.getForObject(url, User.class);

        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        logger.info("Finished Looking up userId " + userId);
        return CompletableFuture.completedFuture(results);
    }

}
