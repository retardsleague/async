package hello;

import hello.domain.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class PostLookupService {

    private static final Logger logger = LoggerFactory.getLogger(PostLookupService.class);

    @Autowired
    private RestTemplate restTemplate;
    

    @Async
    public CompletableFuture<Post> findPost(Long postId) {
        logger.info("Strarted Looking up postId " + postId);
        String url = String.format("https://jsonplaceholder.typicode.com/posts/%s", postId);
        Post results = restTemplate.getForObject(url, Post.class);

        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        logger.info("Finished Looking up postId " + postId);
        return CompletableFuture.completedFuture(results);
    }

}
